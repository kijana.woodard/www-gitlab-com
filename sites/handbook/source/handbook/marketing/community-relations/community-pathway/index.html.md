---
layout: handbook-page-toc
title: "Community Learning Pathway: Course Resources"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Community Learning Pathway

The Community Learning pathway is a course on the GitLab Learn platform, designed to educate GitLab team members and the community about GitLab's community pograms, our commitment to the community and how to engage the community team. At the end of the course, learners earn a badge showing they have a good understanding of the GitLab community and how to engage.

### Course Objectives

At the end of this course, we expect team members to:

1. Understand the value of community to GitLab and how important it is to our strategy
1. Take away best practices around how to interact with our community
1. Understand the various programs we run and where to find more information about them
1. Understand how and when to collaborate with us

The GitLab Community is also able to:

1. Understand GitLab's business model and how it differs from competitors
1. Learn about all of the programs and resources we have to support them
1. Learn tips for how to interact in our community
1. Learn how to collaborate and where to find more info
1. Understand who to reach out to and when

### Course Outline

1. Introduction
1. How community fits into GitLab's strategy
1. GitLab's community programs
1. Open source community best practices
1. Join us

### Introduction Video

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/JGiwxKnxeRw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>


### How community fits into GitLab's strategy

GitLab is the DevOps Platform, made possible by contributions from the community. The GitLab community is composed of our paid and free users, contributors, team members and anyone in the general community who has interacted with GitLab in whatever form. Learn more about our strategy in the links below.

* [GitLab's strategy & open core model](https://about.gitlab.com/company/stewardship/#how-open-source-benefits-from-open-core)
* Dual flywheel strategy and the value of community
  * [Dual Flywheel strategy](https://about.gitlab.com/company/strategy/#dual-flywheels)
  * [MRARR](https://about.gitlab.com/handbook/engineering/quality/performance-indicators/#mrarr)
  * [Percent of MRs from Community](https://about.gitlab.com/handbook/engineering/quality/performance-indicators/#percent-of-mrs-from-community)
  * [Unique Community Contributors Per Month](https://about.gitlab.com/handbook/engineering/quality/performance-indicators/#unique-community-contributors-per-month)
  * [Percent of Feature Community Contributions](https://about.gitlab.com/handbook/engineering/quality/performance-indicators/#percent-of-feature-community-contribution-mrs)
  * [Wider Community merged MRs per release](https://about.gitlab.com/handbook/marketing/community-relations/performance-indicators/#wider-community-merged-mrs-per-release)
  * [Developer Evangelism Monthly Impressions](https://about.gitlab.com/handbook/marketing/community-relations/performance-indicators/#developer-evangelism-monthly-impressions)
  * [What will GitLab Learn platform do for the GitLab community?](https://about.gitlab.com/handbook/people-group/learning-and-development/gitlab-learn/#what-will-gitlab-learn-do-for-the-gitlab-community)

### GitLab's community programs

#### How our community is structured

Ensuring everyone can contribute requires a concious effort to ensure the community is enabled and supported with the right tools they need, thats where the Community team at GitLab plays a crucial role with programs that target different parts of teh community. From the Open source community to the academia, startups and individual contributors, the team supports them in various ways. You can learn more about the individual programs in the section below.

#### Community Programs
* [Contributor Program](https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/)
* Free community programs:
  * [OSS](https://about.gitlab.com/handbook/marketing/community-relations/opensource-program/)
  * [Developer Evangslism](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/)
  * [Education Program](https://about.gitlab.com/handbook/marketing/community-relations/education-program/)
  * [StartUps Program](https://about.gitlab.com/handbook/marketing/community-relations/startups-program/)
* Meetups and evangelism
  * [Evangelist Program](https://about.gitlab.com/handbook/marketing/community-relations/evangelist-program/)
* [GitLab Diversity Scholarship program](https://about.gitlab.com/community/sponsorship/)


### Open source community best practices

* [Fostering DIB in our community](https://about.gitlab.com/handbook/values/#diversity-inclusion)
* [Transparency and openness](https://about.gitlab.com/handbook/values/#transparency)


### Join us

* How to collaborate with the Community Relations team
  * [Project management](https://about.gitlab.com/handbook/marketing/community-relations/project-management/)
  * [Community Response](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/community-response/)
* Get started contributing to GitLab
  * [Developer Portal](https://developer.gitlab.com/)
